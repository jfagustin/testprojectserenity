package com.testprojSerenity.tasks;

import com.testprojSerenity.ui.ToolsQaPracticeForm;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;

public class SelectFrom {

	public static Performable profession (String profession) {
		return Tasks.instrumented(SelectFromOptions.class,ToolsQaPracticeForm.PROFESSION, profession);
	}
	
	public static Performable tools (String tools) {
		return Tasks.instrumented(SelectFromOptions.class,ToolsQaPracticeForm.TOOLS, tools);
	}
	
	public static Performable continents (String continent) {
		return Tasks.instrumented(SelectFromOptions.class,ToolsQaPracticeForm.CONTINENTS, continent);
	}
	
}
