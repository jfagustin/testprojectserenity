package com.testprojSerenity.tasks;

import com.testprojSerenity.ui.GoogleSearchPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

public class OpenGoogleApplication implements Task {

    GoogleSearchPage googleSearchPage;

    @Step("Open the Google web application")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(googleSearchPage)
        );
    }
}
