package com.testprojSerenity.tasks;

import com.testprojSerenity.testrunner.TestRunner;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class Navigate extends TestRunner implements Task{

	private String url;
	
	public Navigate (String url) {
		this.url = url;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		herBrowser.navigate().to(url);
		
	}
	
	public static Performable toGoogleSearchPage () {
		return Tasks.instrumented(Navigate.class, "http://www.google.co.uk");
	}
	
	public static Performable toToolsQaPracticeForm () {
		return Tasks.instrumented(Navigate.class, "https://www.toolsqa.com/automation-practice-form/");
	}
}
