package com.testprojSerenity.tasks;

import com.testprojSerenity.ui.ToolsQaPracticeForm;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

public class OpenToolsQaPracticeForm implements Task{
	
	ToolsQaPracticeForm toolsQaPracticeForm;

    @Step("Open the Google web application")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(toolsQaPracticeForm)
        );
    }
}
