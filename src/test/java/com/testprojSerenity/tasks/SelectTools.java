package com.testprojSerenity.tasks;

import java.util.List;

import com.testprojSerenity.ui.ToolsQaPracticeForm;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class SelectTools implements Task {
String value_to_select;
	
	public SelectTools(String tools) {
		this.value_to_select = tools;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		List<WebElementFacade> element = ToolsQaPracticeForm.TOOLS.resolveAllFor(actor);
		
		System.out.println("Size: " + element.size());
		System.out.println("Value to select: " + value_to_select);
		
		for (WebElementFacade element_to_click : element) {
			System.out.println(element_to_click.getValue());
			if (element_to_click.getValue().trim().contains(value_to_select)){
				actor.attemptsTo(Click.on(element_to_click));
				System.out.println("Selected: " + element_to_click.getValue());
				break;
			}
		}
		
	}
}
