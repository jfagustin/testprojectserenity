package com.testprojSerenity.tasks;

import java.util.List;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;

public class SelectFromOptions implements Task {
	String value_to_select;
	Target target;
	public SelectFromOptions(Target target, String profession) {
		this.value_to_select = profession;
		this.target = target;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		List<WebElementFacade> element = target.resolveAllFor(actor);
		
		System.out.println("Size: " + element.size());
		System.out.println("Value to select: " + value_to_select);
		
		for (WebElementFacade element_to_click : element) {
			System.out.println(element_to_click.getValue());
			if (element_to_click.getValue().trim().contains(value_to_select)){
				actor.attemptsTo(Click.on(element_to_click));
				System.out.println("Selected: " + element_to_click.getValue());
				break;
			}
		}
		
	}
}
