package com.testprojSerenity.interactions;

import com.testprojSerenity.ui.ToolsQaPracticeForm;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;

public class WaitFor {

	public static Performable toolsQaMainForm () {
		return Tasks.instrumented(ElementToBeVisible.class, ToolsQaPracticeForm.MAIN_FORM);
	}
	
}
