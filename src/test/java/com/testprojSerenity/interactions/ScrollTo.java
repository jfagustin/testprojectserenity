package com.testprojSerenity.interactions;

import com.testprojSerenity.testrunner.TestProject_MobiletestAutoRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.targets.Target;

public class ScrollTo implements Task {

	private Target target;
	
	public ScrollTo (Target target) {
		this.target = target;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		while (!target.resolveFor(actor).isCurrentlyVisible()) {
			try {
				TestProject_MobiletestAutoRunner.helper.getDriver().testproject().scrollWindow(0,300);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}
	
}
