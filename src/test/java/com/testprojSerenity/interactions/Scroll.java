package com.testprojSerenity.interactions;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.targets.Target;

public class Scroll {
	
	public static Performable toElement (Target element) {
		return Tasks.instrumented(ScrollTo.class, element);
	}
	
}
