package com.testprojSerenity.interactions;

import java.util.concurrent.TimeUnit;

import org.seleniumhq.jetty9.util.log.Slf4jLog;

import com.testprojSerenity.testrunner.TestRunner;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.targets.Target;

public class ElementToBeVisible extends TestRunner implements Task{
	
	public static Slf4jLog logger;
	
	Target target;
	int max_timeout = 20;

	public ElementToBeVisible(Target target) {
		this.target = target;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor){
		try {
			WebElementFacade element = target.resolveFor(actor);
			
			element.waitUntilPresent().withTimeoutOf(max_timeout, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.debug("Error : " + e.getMessage());
		}
	}
}
