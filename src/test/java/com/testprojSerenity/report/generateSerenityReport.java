package com.testprojSerenity.report;

import java.io.File;
import java.io.IOException;

import net.thucydides.core.Thucydides;
import net.thucydides.core.reports.html.HtmlAggregateStoryReporter;

@SuppressWarnings("deprecation")
public class generateSerenityReport {

	public void execute () {
		File sourceDirectory = new File("target/site/serenity/");
		HtmlAggregateStoryReporter reporter = new HtmlAggregateStoryReporter(Thucydides.getDefaultProjectKey());
		reporter.setOutputDirectory(sourceDirectory);
		    try {
		        reporter.generateReportsForTestResultsFrom(sourceDirectory);
		    }
		    catch (IOException e)
		    {
		        System.out.println("generateReportsForTestResultsFrom exception ");
		        e.printStackTrace();
		    }
	}
	
}
