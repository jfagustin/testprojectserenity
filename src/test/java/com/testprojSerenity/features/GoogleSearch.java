package com.testprojSerenity.features;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import com.testprojSerenity.ui.SearchBox;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GoogleSearch implements Task {

    private String searchTerm;

    protected GoogleSearch(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    @Step("Search for #searchTerm")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(searchTerm)
                     .into(SearchBox.SEARCH_FIELD)
//                     .thenHit(ENTER)
        );
        
        actor.attemptsTo(Click.on(SearchBox.GO_BUTTON));
    }

    public static GoogleSearch forTheTerm(String searchTerm) {
        return instrumented(GoogleSearch.class, searchTerm);
    }

}
