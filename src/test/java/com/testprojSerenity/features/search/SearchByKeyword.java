package com.testprojSerenity.features.search;

import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;
import static net.serenitybdd.screenplay.GivenWhenThen.when;
import static org.hamcrest.Matchers.containsString;

import com.testprojSerenity.tasks.OpenGoogleApplication;
import com.testprojSerenity.testrunner.TestRunner;
import com.testprojSerenity.features.GoogleSearch;
import com.testprojSerenity.tasks.Navigate;

import io.testproject.java.sdk.v2.tests.helpers.WebTestHelper;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.questions.page.TheWebPage;


public class SearchByKeyword extends TestRunner{
	
    static OpenGoogleApplication openGoogleApplication;
	
    public static void executeTest(WebTestHelper helper) {

    	herBrowser = helper.getDriver();
    	
    	anna.can(BrowseTheWeb.with(herBrowser));
    	
//    	anna.attemptsTo(openGoogleApplication);
    	
//    	herBrowser.navigate().to("http://www.google.com");
    	
    	anna.attemptsTo(Navigate.toGoogleSearchPage());

        when(anna).attemptsTo(GoogleSearch.forTheTerm("BDD In Action"));
        
        then(anna).should(eventually(seeThat(TheWebPage.title(), containsString("BDD In Action"))));

    }

}