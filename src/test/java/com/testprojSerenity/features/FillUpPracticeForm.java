package com.testprojSerenity.features;

import static net.serenitybdd.screenplay.GivenWhenThen.then;
import com.testprojSerenity.interactions.WaitFor;
import com.testprojSerenity.tasks.Navigate;
import com.testprojSerenity.tasks.OpenToolsQaPracticeForm;
import com.testprojSerenity.tasks.SelectFrom;
import com.testprojSerenity.testrunner.TestRunner;
import com.testprojSerenity.ui.ToolsQaPracticeForm;
import com.testprojSerenity.interactions.Scroll;

import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class FillUpPracticeForm extends TestRunner{
	
	static OpenToolsQaPracticeForm openToolsQaPracticeForm;
	
//	public static void executeTest(WebTestHelper helper) {
	public static void executeTest() {
		
//		herBrowser = helper.getDriver();
    	
    	anna.can(BrowseTheWeb.with(herBrowser));
    	
//    	when(anna).attemptsTo(openToolsQaPracticeForm);
    	
//    	herBrowser.navigate().to("https://www.toolsqa.com/automation-practice-form/");
    	
    	anna.attemptsTo(Navigate.toToolsQaPracticeForm(), WaitFor.toolsQaMainForm());
    	
//    	helper.getDriver().testproject().scrollWindow(0,300);

//    	anna.attemptsTo(Scroll.to(ToolsQaPracticeForm.FIRST_NAME));
    	
    	anna.attemptsTo(Scroll.toElement(ToolsQaPracticeForm.FIRST_NAME));
    	
    	then(anna).attemptsTo(Enter.theValue("MyFirstName").into(ToolsQaPracticeForm.FIRST_NAME));
    	
    	anna.attemptsTo(Scroll.toElement(ToolsQaPracticeForm.LAST_NAME));
    	
    	then(anna).attemptsTo(Enter.theValue("MyLastName").into(ToolsQaPracticeForm.LAST_NAME));
    	
    	anna.attemptsTo(Scroll.toElement(ToolsQaPracticeForm.SEX_MALE));
    	
    	then(anna).attemptsTo(Click.on(ToolsQaPracticeForm.SEX_MALE));
    	
    	anna.attemptsTo(Scroll.toElement(ToolsQaPracticeForm.DATE));
    	
    	then(anna).attemptsTo(Enter.theValue("01/01/2019").into(ToolsQaPracticeForm.DATE));
    	
    	anna.attemptsTo(Scroll.toElement(ToolsQaPracticeForm.YEARS_OF_EXPERIENCE));
    	
    	then(anna).attemptsTo(Click.on(ToolsQaPracticeForm.YEARS_OF_EXPERIENCE));
    	
    	anna.attemptsTo(Scroll.toElement(ToolsQaPracticeForm.PROFESSION));
    	
    	then(anna).attemptsTo(SelectFrom.profession("Manual Tester"));
    	
    	anna.attemptsTo(Scroll.toElement(ToolsQaPracticeForm.TOOLS));
    	
    	then(anna).attemptsTo(SelectFrom.tools("QTP"),
    			SelectFrom.tools("Selenium IDE"),
    			SelectFrom.tools("Selenium Webdriver")
    			);
    	
    	anna.attemptsTo(Scroll.toElement(ToolsQaPracticeForm.CONTINENTS));
    	
    	then(anna).attemptsTo(SelectFromOptions.byVisibleText("Antartica").from(ToolsQaPracticeForm.CONTINENTS));
    	
    	then(anna).attemptsTo(SelectFromOptions.byVisibleText("Switch Commands").from(ToolsQaPracticeForm.SELENIUM_COMMANDS));
    	
    	then(anna).attemptsTo(Click.on(ToolsQaPracticeForm.BUTTON));
    	
	}
	
}
