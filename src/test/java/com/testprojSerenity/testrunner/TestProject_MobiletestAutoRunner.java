package com.testprojSerenity.testrunner;

import io.testproject.java.enums.AutomatedBrowserType;
import io.testproject.java.sdk.v2.Runner;
import io.testproject.java.sdk.v2.enums.BrowserName;
import io.testproject.java.sdk.v2.internal.AgentProxy;
import io.testproject.java.sdk.v2.reporters.TestReporter;
import io.testproject.java.sdk.v2.tests.helpers.WebTestHelper;
import net.thucydides.core.annotations.Step;

import java.lang.String;

import org.seleniumhq.jetty9.util.log.Slf4jLog;

public class TestProject_MobiletestAutoRunner extends TestRunner{
	
	public static Slf4jLog logger;
	
//	public static Runner runner;
	
	public static String developerToken = "7RfWMwDbG7SO7-ORGzPliW0a6holmopxzV4qv2CqR0E1";
	
	public static String And_deviceUdid = "29eca8bb1e047ece"; 
	
	public static String iOS_deviceName = "Grove's iPhone";
	
	public static String iOS_deviceUdid = "4f2471f69ac85049982c92eea4f1d44b14d11d79";
	
	public static AutomatedBrowserType browserType = AutomatedBrowserType.Chrome;
	
    public static WebTestHelper helper;

    @Step
    public static void iOS_setup () {
    	try {
    		
			Runner runner = Runner.createIOSWeb(developerToken, iOS_deviceUdid, iOS_deviceName);
			
//			AgentProxy ap = new AgentProxy("https://local.rdr.tpagent.io:8443"); 
			
			AgentProxy ap = new AgentProxy("https://localhost:8443");
			
	    	TestReporter tp = new TestReporter(ap);
	    	
	    	helper = new WebTestHelper(runner.getDriver(), ap, tp);
	    	
	    	herBrowser = helper.getDriver();
	    	
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			logger.debug("Error : " + e.getMessage());
			
		}
    	
    }
    
    @Step
    public static void Android_setup () {
    	try {
    		
			Runner runner = Runner.createAndroidWeb(developerToken, And_deviceUdid, BrowserName.Chrome);//(developerToken, And_deviceUdid,androidPackage, androidActivity);
			
//			AgentProxy ap = new AgentProxy("https://local.rdr.tpagent.io:8443"); 
			
			AgentProxy ap = new AgentProxy("https://localhost:8443");
			
	    	TestReporter tp = new TestReporter(ap);
	    	
	    	helper = new WebTestHelper(runner.getDriver(), ap, tp);
	    	
	    	herBrowser = helper.getDriver();
	    	
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			
		}
    	
    }
	
	
	
	
//	public static void main(String[] args) {
//	  try {
//	    runtoolsQASample();
//	    generateReport();
//	  }
//	  catch (Exception e) {
//	  }
//	}
//	
//	/**
//	 * Runs "toolsQASample"
//	 */
//	public static StepExecutionResult runtoolsQASample() throws Exception {
//	   runner = Runner.createIOSWeb(developerToken, deviceUdid, deviceName);
//	// runner = Runner.createWeb(developerToken, browserType);
//	    
//	// SearchByKeywordStory sbks = new SearchByKeywordStory(runner.getDriver());
//	    
//	   return runner.run(new SearchByKeywordStory());
//	      
//	}
//	  
//	public static void generateReport () {
//		generateSerenityReport generateReport = new generateSerenityReport();
//		generateReport.execute();
//	}
}
