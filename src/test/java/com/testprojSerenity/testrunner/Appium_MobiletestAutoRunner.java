package com.testprojSerenity.testrunner;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Appium_MobiletestAutoRunner {
	static WebDriver driver;

	public static void Android_setUp() throws MalformedURLException{
		//Set up desired capabilities and pass the Android app-activity and app-package to Appium
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("browserName", "Chrome");
		capabilities.setCapability("platformVersion", "9"); 
		capabilities.setCapability("deviceName","Galaxy S9");
		capabilities.setCapability("platformName","Android");
	 
	   driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	   TestRunner.herBrowser = driver;
	   
	}
}
