package com.testprojSerenity.testrunner;

import java.net.MalformedURLException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.testprojSerenity.features.FillUpPracticeForm;
import com.testprojSerenity.features.search.SearchByKeyword;

import io.testproject.java.sdk.v2.exceptions.FailureException;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;


@RunWith(SerenityRunner.class)
public class TestRunner {

	protected static Actor anna = Actor.named("Anna");
	
    @Before
    public void annaCanBrowseTheWeb() {
        anna.can(BrowseTheWeb.with(herBrowser));
    }

    @Managed(uniqueSession = true)
    public static WebDriver herBrowser;

//    @Test
    public void executeSearchByKeyword() throws FailureException {
    	
    	TestProject_MobiletestAutoRunner.iOS_setup();
    	SearchByKeyword.executeTest(TestProject_MobiletestAutoRunner.helper);
    	
    }
    
    @Test
    public void execute_iOS_FillUpPracticeForm() throws FailureException {
    	
    	TestProject_MobiletestAutoRunner.iOS_setup();
    	FillUpPracticeForm.executeTest();
    	
    }
    
    @Test
    public void execute_Android_FillUpPracticeForm() throws FailureException, MalformedURLException {
    	
    	TestProject_MobiletestAutoRunner.Android_setup(); //for Test Project Setup
//    	Appium_MobiletestAutoRunner.Android_setUp(); //for Appium Setup
    	FillUpPracticeForm.executeTest();
    	
    }

}
