package com.testprojSerenity.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.toolsqa.com/automation-practice-form/")
public class ToolsQaPracticeForm extends PageObject {
	
	public static Target MAIN_FORM = Target.the("Main Form").located(By.cssSelector("div[id='content'][role='main']"));
	public static Target FIRST_NAME = Target.the("First Name textfield").located(By.cssSelector("input[name='firstname']"));
	public static Target LAST_NAME = Target.the("Last Name textfield").located(By.cssSelector("input[name='lastname']"));
	public static Target SEX = Target.the("Sex").located(By.cssSelector("input[name='sex']"));
	public static Target SEX_MALE = Target.the("Sex-Male radiobutton").located(By.cssSelector("input[value='Male']"));
	public static Target SEX_FEMALE = Target.the("Sex-Male radiobutton").located(By.cssSelector("input[value='Female']"));
	public static Target YEARS_OF_EXPERIENCE = Target.the("Years of Experience").located(By.cssSelector("input[name='exp'][value='1']"));
	public static Target DATE = Target.the("Date").located(By.id("datepicker"));
	public static Target PROFESSION = Target.the("Profession").located(By.name("profession"));
	public static Target TOOLS = Target.the("Tools").located(By.name("tool"));
	public static Target CONTINENTS = Target.the("Continents").located(By.id("continents"));
	public static Target SELENIUM_COMMANDS = Target.the("Selenium Commands").located(By.name("selenium_commands"));
	public static Target BUTTON = Target.the("Submit button").located(By.id("submit"));
	
}
