package com.testprojSerenity.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchBox {
    public static Target SEARCH_FIELD = Target.the("search field").located(By.name("q"));
    public static Target GO_BUTTON = Target.the("Go Search button").located(By.cssSelector("button[class='Tg7LZd'][aria-label='Google Search']"));
}